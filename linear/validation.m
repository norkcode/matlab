close all;
clear all;
clc;

% N = 500;
% tt = linspace(0, 8*pi, N);
% A = cos(tt);
% An = awgn(A, 10);

data_file = '../data/test.txt';
data = csvread(data_file);

tt = (data(:,1) - data(1,1)) / 1000;
N = length(tt);
X = tt;
T = mean(diff(tt));
A = cos(tt);
An = data(:,2);


% Kalman filter

% State size
ss = 3;
% Sample size
os = 1;
% Process noise


dT = mean(diff(tt));

F = [
    1 dT 0.5*dT^2;
    0 1 -1*dT;
    0 0 1;
];

H = [
    1 0 0
];

q = 0.1;
r = 0.1;

Q = eye(ss)*q^2;
R = eye(os)*r^2;

x_estimate = zeros(ss, 1);
P = eye(ss)*q;

for t=1:N
    
    % prediction
    x_predition = F*x_estimate;
    P = F*P*F'+Q;
    
    % corretion
    S = H*P*H'+R;
    K = P*H'/S;
    
    x_estimate = x_estimate + K * ( An(t) - H*x_predition);
    P = P - K*H*P;
    
    x_h(:,t) = x_predition;
end

Pnx = cumtrapz(tt, cumtrapz(tt, An));
Px = cumtrapz(tt, cumtrapz(tt, A));
figure;
plot(tt, Px, 'g', tt, Pnx, 'r');
title('Position change');


figure;
plot(tt, A, 'g', tt, An, 'r');
title('Acceleration');

figure;
plot(tt, An, 'g', tt, x_h(1,:), 'r');
title('Filtered acceleration');

figure;
plot(tt, Px, 'g', tt, x_h(3,:), 'r');
title('Filtered position');
