close all;
clear all;
clc;

%% Read the data
data_file = '../data/test.txt';
% data_file = '../data/test.txt';
data = csvread(data_file);

%% Make time unique

tt = (data(:,1) - data(1,1))/1000;
N = length(tt);
X = tt;
dT = mean(diff(tt));

%% Position part

Ax = data(:,2) * 9.86;
%Ay = data(:,3) * 9.86;
%Az = data(:,4) * 9.86;

%Ax = Ax - mean(Ax);
%Ay = Ay - mean(Ay);

%Px = cumtrapz(tt, cumtrapz(tt, Ax));
%Py = cumtrapz(tt, cumtrapz(tt, Ay));
%Pz = cumtrapz(tt, cumtrapz(tt, Az));

%% Kalman filter


% state size
ss = 3; 
% sample size
os = 1;
% Filtered position
% F = [
%    1 0 0 dT 0 0 0.5*dT^2 0 0;
%    0 1 0 0 dT 0 0 0.5*dT^2 0;
%    0 0 1 0 0 dT 0 0 0.5*dT^2;
%    
%    0 0 0 1 0 0 -dT 0 0;
%    0 0 0 0 1 0 0 -dT 0;
%    0 0 0 0 0 1 0 0 -dT;
%    
%    0 0 0 0 0 0 1 0 0;
%    0 0 0 0 0 0 0 1 0;
%    0 0 0 0 0 0 0 0 1;
% ]; 
F = [
   1 dT 0.5*dT^2;
   0 1 -dT;
   0 0 1;
]; 

H = [
   1 0 0;
];
q = 0.1;
r = 0.1;
% Process noise
Q = eye(ss)*q^2;
% measurement noise
R = eye(os)*r^2;

x_estimate = zeros(ss,1);
P = eye(ss)*q;

% Initial Kalman gain
for t=1:N
    A = [Ax(t)];
    
    % Prediction step
    x_predition = F*x_estimate;
    P = F*P*F'+Q;
    
    % Correction step
    S = H*P*H'+R;
    K = P*H'/S;
    
    x_estimate = x_estimate + K * (A - H*x_predition);
    P = P-K*H*P;
    
    x_h(:,t) = x_predition;
    
end
% 
% 
% Pkx = x_h(7,:);
% Pky = x_h(8,:);
% Pkz = x_h(9,:);

%% Plot sensor axis

% figure;
% subplot(3,1,1);
% plot(Ax);
% hold on;
% plot(Pkx, 'r');
% grid on;
% hold off;
% 
% subplot(3,1,2);
% plot(Ay);
% hold on;
% plot(Pky, 'r');
% grid on;
% hold off;
% 
% subplot(3,1,3);
% plot(Az);
% hold on;
% plot(Pkz, 'r');
% grid on;
% hold off;

%% Plot position

% figure;
% plot(tt, Ax, 'k');
% xlabel('t, s');
% ylabel('a, m/s^2');
% 
% figure;
% plot(tt, cumtrapz(tt, cumtrapz(tt, Ax)), 'k');
% xlabel('t, s');
% ylabel('distance, m');


Vx = cumtrapz(tt, Ax);
Px = cumtrapz(tt, Vx);

figure;
plot(tt, Px, 'k-', tt, x_h(3,:), 'k+');
legend('Original', 'Linear Kalman');
xlabel('t, s');
ylabel('distance, m');

csvwrite('../../research/data/linear_kalman.csv', [tt Ax Vx Px x_h(3,:)']);

%disp('Px mean');
%mean(Px)
%disp('Px std');
%std(Px)

%disp('x_h mean');
%mean(x_h(7,:))
%%disp('x_h std');
%std(x_h(7,:))

%xcorr(Px,x_h(7,:),0,'coeff')

%figure;
%plot(x_h(7,:), x_h(8,:));

% subplot(1,2,1);
% plot(tt, Ax);
% ylabel('distance, m');
% xlabel('t, s');
% title('original position');
% 
% subplot(1,2,2);
% plot(tt, Pkx);
% title('filtered position');
% ylabel('distance, m');
% xlabel('t, s');

% figure('Name', 'Original data');
% subplot(3,1,1);
% plot(tt, Px);
% grid on;
% xlabel('time, n');
% ylabel('distance, cm');
% 
% subplot(3,1,2);
% plot(tt, Py);
% xlabel('time, n');
% ylabel('distance, cm');
% grid on;
% hold off;
% 
% subplot(3,1,3);
% plot(Px, Py);
% xlabel('distance, cm');
% ylabel('distance, cm');
% grid on;
% hold off;
% 
% figure('Name', 'Filtered data');
% subplot(3,1,1);
% plot(tt, x_h(1,:));
% grid on;
% xlabel('time, n');
% ylabel('distance, cm');
% 
% subplot(3,1,2);
% plot(tt, x_h(2,:));
% grid on;
% xlabel('time, n');
% ylabel('distance, cm');
% 
% subplot(3,1,3);
% plot(x_h(1,:), x_h(2,:));
% xlabel('distance, cm');
% ylabel('distance, cm');
% grid on;
% hold off;
