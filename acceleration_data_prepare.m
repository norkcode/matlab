close all;
clear all;
clc;

data_file = 'data/20170528_15_34_01.txt';
data = csvread(data_file);
tt = (data(:,1) - data(1,1)) / 1000;
Ax = data(:,2) * 9.86;
Kx = data(:,4);

figure('Name', 'Acceleration data x axis');
plot(tt, Ax, 'k');
xlabel('t, s');
ylabel('m/s^2');
grid on;

figure('Name', 'Velocity data x axis');
plot(tt, cumtrapz(tt, Ax), 'k');
xlabel('t, s');
ylabel('m/s');
grid on;

figure('Name', 'Distance data x axis');
plot(tt, cumtrapz(tt, cumtrapz(tt,Ax)), 'k-', tt, cumsum(Kx), 'k+');
xlabel('t, s');
ylabel('m');
grid on;

Vx = cumtrapz(tt, Ax);
Px = cumtrapz(tt, Vx);

csvwrite('../research/data/acceleration.csv', [tt Ax Vx Px]);