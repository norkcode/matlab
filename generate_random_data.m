clear all;
close all;
clc;

N = 1000;
tt = linspace(0, 4*pi, N);
A = cos(tt);
Axn = awgn(A, 10);
Ayn = awgn(A, 10);
Azn = awgn(A, 10);

data = [tt*1000; Axn; Ayn; Azn];


csvwrite('data/test.txt', data');


plot(tt, Axn);