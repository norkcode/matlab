close all;
clear all;
clc;

%% Read the data
data_file = '../data/test.txt';
data = csvread(data_file);

%% Make time unique

tt = (data(:,1) - data(1,1))/1000;
N = length(tt);
X = tt;
dT = mean(diff(tt));

%% Position part
g = 9.84;

Ax = data(:,2)*g;

Px = cumtrapz(X, cumtrapz(X, Ax));

%% Particle filter

% Number of states
nx = 3;
q = 0.2;

sys = @(k, x, uk) [
    x(1)+x(2)+x(3)*0.5*dT^2;
    x(2)+x(3)*dT;
    x(3);
] + uk;

% observation equation
ny = 1;
obs = @(k, x, vk) x(3) + vk;

% PDF of process noise and noise generation function
sigma_u = ones(nx)*q^2;

% size of the vetor of process noise
nu = size(sigma_u, 1);
p_sys_noise = @(u) mvnpdf(u, zeros(1,nu), sigma_u);
% sample from p_sys_noise
gen_sys_noise = @(u) mvnrnd(zeros(1,nu), sigma_u, 1)';

% PDF of observation noise and noise generator function
sigma_v = 0.8^2;
% size of the vector of observation noise
nv = size(sigma_v, 1);
p_obs_noise = @(v) mvnpdf(v, zeros(1,nv), sigma_v);
% sample from p_obs_noise
gen_obs_noise = @(v) mvnrnd(zeros(1,nv), sigma_v, 1)';

% Initial pdf
gen_x0 = @(x) mvnrnd(zeros(1, nu), sigma_u, 1)';

% observation likelihood pdf
p_yk_given_xk = @(k, yk, xk) p_obs_noise(yk - obs(k, xk, 0));

pf.k = 1; % initial iteration number
pf.Ns = 1000; % number of particles
pf.w = zeros(pf.Ns, N); % weights
pf.particles = zeros(nx, pf.Ns, N); % particles
pf.gen_x0 = gen_x0; % function for sampling from initial pdf p_x0
pf.p_yk_given_xk = p_yk_given_xk;
pf.gen_sys_noise = gen_sys_noise;

xh0 = zeros(nu,1);

% initialization
xh = zeros(nu, N); 
xh(:,1) = xh0;
yh = zeros(ny, N); 
yh(:,1) = obs(1, xh0, 0);

% Estimate state
for k = 2:N
    % state estimation
    pf.k = k;
    
    y_k = Ax(k);
    
    [xh(:,k), pf] = particle_filter(sys, y_k, pf, 'SIR');
    
    % filter observation
    yh(:,k) = obs(k, xh(:,k), 0);
end

disp(xh);
%% Plot
figure;
plot(tt,Ax,'g',tt,xh(3,:), 'r');
legend('Original', 'Filtered');

figure;
plot(tt,Px,'g', tt, xh(1,:)/100, 'r');

