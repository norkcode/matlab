close all;
clear all;
clc;

N = 500;
tt = linspace(0, 4*pi, N);
A = cos(tt);
An = awgn(A, 0.1);

% Kalman filter

% State size
ss = 3;
% Sample size
os = 1;
% Process noise


dT = mean(diff(tt))/40;

f = @(x) [
    x(1) + x(2) + x(3)*0.5*dT^2;
    x(2) + x(3)*dT;
    x(3)
];

h = @(x) x(3);

q = 0.1;
r = 0.5;

Q = eye(ss)*q^2;
R = eye(os)*r^2;
P = eye(ss)*q;

nx = size(Q,1);
nz = size(R,1);
m = zeros(nx, 1);

kappa = 3 - nx;
W = [ kappa/(kappa+nx) 0.5/(kappa+nx)+zeros(1,2*nx) ];
c = sqrt(kappa + nx);

% Space for the estimates
MM = zeros(nx, N);
PP = zeros(nx, nx, N);

for i = 1:N
    Z_r = An(i);
    % Filtering steps
    X = sigma_points(m, P, c);
    [m_hat, X_hat, P_hat, X_dev] = ut(f, X, W, nx, Q);
    % Update steps
    [z_hat, Z_Hat, P_zz, Z_dev] = ut(h, X_hat, W, nz, R);
    P_xz = X_dev * diag(W) * Z_dev';
    S = R + P_zz;
    K = P_xz / S;
    m = m_hat + K *(Z_r - z_hat);
    P = P_hat - P_xz * K';
    MM(:,i) = m;
    PP(:,:,i) = P;
end

Pnx = cumtrapz(tt, cumtrapz(tt, An));
Px = cumtrapz(tt, cumtrapz(tt, A));
figure;
plot(tt, Px, 'g', tt, Pnx, 'r');
title('Position change');


figure;
plot(tt, A, 'g', tt, An, 'r');
title('Acceleration');

figure;
plot(tt, An, 'g', tt, MM(3,:), 'r*');
title('Filtered acceleration');

figure;
plot(tt, Px, 'g', tt, MM(1,:), 'r');
title('Filtered position');

% figure;
% plot(tt,MM(3,:), tt, MM(2,:));