function X=sigma_points(m,P,c)
%Sigma points around reference point
%Inputs:
%       x: reference point
%       P: covariance
%       c: coefficient
%Output:
%       X: Sigma points

cP = chol(P);
% disp('cP');
% disp(cP);
A = c*cP';
Y = m(:,ones(1,numel(m)));
% disp('Y');
% disp(Y);
% disp('Y+A');
% disp(Y+A);
X = [m Y+A Y-A];
end