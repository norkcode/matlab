close all;
clear all;
clc;

%% Read the data
%data_file = '../data/test.txt';
data_file = '../data/20170528_21_05_39.txt';
data = csvread(data_file);

%% Make time unique
%tt_N = 20;
tt = (data(:,1) - data(1,1)) / 1000; % ms -> s
N = length(tt);
X = tt;
dT = mean(diff(tt))/100;

%% Position part

Ax = data(:,2) * 9.86;
%Ay = data(:,3) * 9.86;
%Az = data(:,4) * 9.86;

%K1 = data(:,3);
%K2 = data(:,4);
%K3 = data(:,5);

Ax = Ax - mean(Ax);
%Ay = Ay - mean(Ay);



Px = cumtrapz(X, cumtrapz(X, Ax));
%Py = cumtrapz(X, cumtrapz(X, Ay));
%Pz = cumtrapz(X, cumtrapz(X, Az));

%% System

% nonlinear state equations
f = @(x)[
    x(1) + x(2) - x(3)*0.5*dT^2;
    x(2) + x(3)*dT;
    x(3);
];

% measurement equation
h = @(x)[ x(3); ];

%% Unscented Kalman filter

% number of state
n = 3; 
% number of measurement
ss = 1;
% std of process
q = 0.1;
% std of measurement
r = 0.1; 
% covariance of process
Q = eye(n)*q^2;
% covariance of measurement
R = eye(ss)*r^2;

nx = size(Q,1);
nz = size(R,1);
m = zeros(nx, 1);
P = eye(n)*q;

kappa = n - nx;
W = [ kappa/(kappa+nx) 0.5/(kappa+nx)+zeros(1,2*nx) ];
c = sqrt(kappa + nx);

% Space for the estimates
MM = zeros(nx, N);
PP = zeros(nx, nx, N);

for i = 1:N
    Z_r = [Ax(i)]';
    % Filtering steps
    X = sigma_points(m, P, c);
    
    [m_hat, X_hat, P_hat, X_dev] = ut(f, X, W, nx, Q);
    % Update steps
    [z_hat, Z_Hat, P_zz, Z_dev] = ut2(h, X_hat, W, nz, R);
    
    P_xz = X_dev * diag(W) * Z_dev';
    S = R + P_zz;
    K = P_xz * inv(S);
    m = m_hat + K *(Z_r - z_hat);
    P = P_hat - P_xz * K';
       
    MM(:,i) = m;
    PP(:,:,i) = P;
end

figure;
plot(tt, Ax, 'g', tt, MM(3,:), 'r');

figure;
plot(tt, Px, '-', tt, MM(1,:), '+');


csvwrite('../../presentation/data/unscented_kalman_matlab.csv', [tt Px MM(1,:)']);

% plot(tt, Px, 'k-', tt, K1, 'k+');
% legend('Original', 'Unscented Kalman', 'MPU');
% xlabel('t, s');
% ylabel('distance, m');
% 
% disp('Px mean');
% mean(Px)
% disp('Px std');
% std(Px)
% 
% disp('MM mean');
% mean(MM(1,:))
% disp('MM std');
% std(MM(1,:))
% 
% disp('xcorr');
% xcorr(MM(1,:), Px, 0, 'coeff')

% plot(MM(5,:), MM(6,:));
% xlabel('x');
% ylabel('y');