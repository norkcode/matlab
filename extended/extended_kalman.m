close all;
clear all;
clc;

%% Read the data
data_file = '../data/test.txt';
data = csvread(data_file);

%% Make time unique

tt = (data(:,1) - data(1,1)) / 1000;
N = length(tt);
X = tt;
dT = mean(diff(tt))/100;

%% Position part

Ax = data(:,2) * 9.86;
Ay = data(:,3) * 9.86;
Az = data(:,4) * 9.86;

Ax = Ax - mean(Ax);
Ay = Ay - mean(Ay);

Px = cumtrapz(X, cumtrapz(X, Ax));
Py = cumtrapz(X, cumtrapz(X, Ay));
Pz = cumtrapz(X, cumtrapz(X, Az));

%% Kalman filter

% number of state
n = 3; 
% number of measurement
ss = 1;
% std of process
q = 0.1;
% std of measurement
r = 0.1; 
% covariance of process
Q = eye(n)*q^2;
% covariance of measurement
R = eye(ss)*r^2;
% nonlinear state equations
f = @(x)[
    x(1) + x(2) + x(3)*0.5*dT^2;
    x(2) + x(3)*dT;
    x(3)
];

% measurement equation
h = @(x) x(3);
% initial state
s = zeros(n, 1);
% initial state with noise
x = s;
P = eye(n)*q;

xV = zeros(n,N);
sV = zeros(n,N);
zV = zeros(ss,N);

for k = 1:N
    % measurements
    z = Ax(k);
    % save actual state
    sV(:,k) = s;
    % save measurement
    zV(:, k) = z;
    
    % EFK
    [x1, F] = jaccsd(f,x);
    [z1, H] = jaccsd(h, x1);
    
    P = F*P*F'+Q;
    
    S = H*P*H'+R;
    K = P*H'/S;
    x = x1 + K*(z-z1);
    P = P - K*H*P;
    
    % save estimate
    xV(:,k) = x;
    % update process
    s = f(s);
end

%% Plot position

figure;
plot(tt, Ax, 'g', tt, xV(3,:), 'r');

figure;
plot(tt, Px, 'k-', tt, xV(1,:), 'k+');
legend('Original', 'Extended Kalman');
xlabel('t, s');
ylabel('distance, m');

disp('Px mean');
mean(Px)
disp('Px std');
std(Px)

disp('xV mean');
mean(xV(1,:))
disp('xV std');
std(xV(1,:))

xcorr(Px, xV(1,:), 0, 'coeff')

csvwrite('../../research/data/extended_kalman.csv', [tt Px xV(1,:)']);

% figure('Name', 'Original data');
% subplot(3,1,1);
% plot(tt, Px);
% grid on;
% xlabel('time, n');
% ylabel('distance, cm');
% 
% subplot(3,1,2);
% plot(tt, Py);
% xlabel('time, n');
% ylabel('distance, cm');
% grid on;
% hold off;
% 
% subplot(3,1,3);
% plot(Px, Py);
% xlabel('distance, cm');
% ylabel('distance, cm');
% grid on;
% hold off;
% 
% figure('Name', 'Filtered data');
% subplot(3,1,1);
% plot(tt, xV(5,:));
% grid on;
% xlabel('time, n');
% ylabel('distance, cm');
% 
% subplot(3,1,2);
% plot(tt, xV(6,:));
% grid on;
% xlabel('time, n');
% ylabel('distance, cm');
% 
% subplot(3,1,3);
% plot(xV(1,:), xV(2,:));
% xlabel('distance, cm');
% ylabel('distance, cm');
% grid on;
% hold off;

%% Plot filtered data
% figure;
% subplot(2,1,1);
% plot(tt, Ax, 'b', tt, xV(1,:), 'g');
% subplot(2,1,2);
% plot(tt, Ay, 'b', tt, xV(2,:), 'g');
